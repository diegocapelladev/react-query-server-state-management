import Posts from './Posts'

import './App.css'

function App() {
  return (
    <div className="App">
      <h1>Blog Ipsum</h1>
      <Posts />
    </div>
  )
}

export default App
