import { useQuery } from '@tanstack/react-query'
import { PostType } from './Posts'

type PostDetailType = {
  post: PostType
}

type PostCommentsType = {
  id: number
  postId: number
  name: string
  email: string
  body: string
}

async function fetchComments(id: number) {
  const response = await fetch(
    `https://jsonplaceholder.typicode.com/comments/${id}`,
  )

  return response.json()
}

export default function PostDetail({ post }: PostDetailType) {
  const {
    data: comments,
    isLoading,
    isError,
    error,
  } = useQuery<PostCommentsType>({
    queryKey: ['comments', post.id],
    queryFn: () => fetchComments(post.id),
  })

  if (isLoading) {
    return <p>Loading...</p>
  }

  if (isError) {
    return <p>Error: {error.messages}</p>
  }

  return (
    <>
      <h1>{post.title}</h1>
      <p>{post.body}</p>
      <h2>Comments</h2>
      <span>{comments.name}</span>
      <p>{comments.body}</p>
      <span>{comments.email}</span>
    </>
  )
}
