import { useEffect, useState } from 'react'
import { useQuery, useQueryClient } from '@tanstack/react-query'

import PostDetail from './PostDetail'

export type PostType = {
  id: number
  userId: number
  title: string
  body: string
}

const maxPostPage = 10

async function fetchPosts(pageNumber: number): Promise<PostType[]> {
  const response = await fetch(
    `https://jsonplaceholder.typicode.com/posts?_limit=10&_page=${pageNumber}`,
  )

  return response.json()
}

export default function Posts() {
  const [currentPage, setCurrentPage] = useState(1)
  const [selectedPost, setSelectedPost] = useState<PostType | null>(null)

  const queryClient = useQueryClient()

  const { data, isLoading, isError, error, isPreviousData } = useQuery<
    PostType[]
  >({
    queryKey: ['posts', currentPage],
    queryFn: () => fetchPosts(currentPage),
    staleTime: 10000,
    keepPreviousData: true,
  })

  useEffect(() => {
    if (!isPreviousData) {
      const nextPage = currentPage + 1

      // queryClient.prefetchQuery(['posts', nextPage], () => fetchPosts(nextPage))
      queryClient.prefetchQuery({
        queryKey: ['posts', nextPage],
        queryFn: () => fetchPosts(nextPage),
      })
    }
  }, [currentPage, queryClient, isPreviousData])

  if (isLoading) {
    return <p>Loading...</p>
  }

  if (isError) {
    return <p>Error: {error.message}</p>
  }

  return (
    <>
      <ul>
        {data?.map((post) => (
          <li
            key={post.id}
            className="post-title"
            onClick={() => setSelectedPost(post)}
          >
            {post.title}
          </li>
        ))}
      </ul>

      <div className="pages">
        <button
          disabled={currentPage <= 1}
          onClick={() => {
            setCurrentPage((prev) => prev - 1)
          }}
        >
          Previous page
        </button>

        <span>Page: {currentPage}</span>

        <button
          disabled={currentPage >= maxPostPage}
          onClick={() => {
            setCurrentPage((prev) => prev + 1)
          }}
        >
          Next page
        </button>
      </div>
      <hr />
      {selectedPost && <PostDetail post={selectedPost} />}
    </>
  )
}
