# Get Started

```sh
npm install
# or
yarn install
```
## Run

```sh
npm run dev
# or
yarn dev
```

## Eslint configured

[@rocketseat/eslint-config](https://www.npmjs.com/package/@rocketseat/eslint-config?activeTab=readme)

## React Query / TanStack Query
[TanStack](https://tanstack.com/query/latest/docs/react/overview)